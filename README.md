## Languages and Libraries 

- Kotlin
- Constraint Layout
- Material components
- Jetpack Navigation
- Dependency Injection by (Koin)
- Retrofit
- OkHttp3
- Android Lifecycle extensions
- Reactive Programming (RxJava)
- Glide (async image loading)

## Architecture
The architecture of the app is based on MVVM. The motivation to use handful of libraries is to write production ready code that can easily be extended with more features in future. The design based on Clean Architecture by Uncle Bob. Separation of concerns and single responsibility principle can easily be visualized by looking at the code. Unit tests has been written to demonstrate a testable `ViewModel`. The idea of "Offensive Programming" can be seen in `OfferProviderImpl`.

## Improvements
- More unit and UI tests
 

