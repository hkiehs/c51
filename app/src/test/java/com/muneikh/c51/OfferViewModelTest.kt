package com.muneikh.c51

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.muneikh.c51.offer.model.Offer
import com.muneikh.c51.offer.provider.OfferProvider
import com.muneikh.c51.offer.viewModel.OfferViewModel
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class OfferViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var offerProvider: OfferProvider

    private lateinit var impl: OfferViewModel

    @Mock
    private lateinit var mockOffersObserver: Observer<List<Offer>>

    private val testScheduler = TestScheduler()

    private val offers = mutableListOf(Offer("Rice", 1, "someUrl", 12.0))

    @Before
    fun setUp() {
        `when`(offerProvider.offers).thenReturn(Single.just(offers))
        impl = OfferViewModel(offerProvider)
    }

    @Test
    fun getOffers_offersReceived() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }

        impl.offers.observeForever(mockOffersObserver)
        verify(mockOffersObserver).onChanged(offers)
    }
}
