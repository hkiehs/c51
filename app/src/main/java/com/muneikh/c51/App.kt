package com.muneikh.c51

import android.app.Application
import com.muneikh.c51.di.appModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }


    companion object {
        lateinit var instance: App
    }
}
