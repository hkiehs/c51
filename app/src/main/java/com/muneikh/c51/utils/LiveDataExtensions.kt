package com.muneikh.c51.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import io.reactivex.Single

fun <T> Single<T>.toLiveData(): LiveData<T> =
        LiveDataReactiveStreams.fromPublisher(this.toFlowable())