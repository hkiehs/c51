package com.muneikh.c51.api

import com.muneikh.c51.offer.model.OfferModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface Api {
    @GET("/bins/11nf5w")
    fun getOffers(): Single<Response<OfferModel>>
}
