package com.muneikh.c51.offer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.muneikh.c51.R
import com.muneikh.c51.offer.viewModel.OfferViewModel
import kotlinx.android.synthetic.main.fragment_offer.*
import org.koin.android.viewmodel.ext.android.viewModel

class OfferFragment : Fragment() {

    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(activity!!)
    }

    private val offerAdapter: OfferAdapter by lazy {
        OfferAdapter()
    }

    private val offerViewModel: OfferViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_offer, container, false)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = offerAdapter

        bindViewModel()
    }

    private fun bindViewModel() {
        offerViewModel.offers.observe(this, Observer {
            offerAdapter.clear()
            offerAdapter.addAll(it)
        })
    }
}
