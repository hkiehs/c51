package com.muneikh.c51.offer.viewModel

import com.muneikh.c51.offer.provider.OfferProvider
import com.muneikh.c51.utils.AutoDisposingViewModel
import com.muneikh.c51.utils.toLiveData

class OfferViewModel(offerProvider: OfferProvider) : AutoDisposingViewModel() {
    val offers = offerProvider.offers.toLiveData()
}