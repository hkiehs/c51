package com.muneikh.c51.offer.model

import com.google.gson.annotations.SerializedName

data class OfferModel(val batchId: Int, val offers: List<Offer>)
data class Offer(
        val name: String,
        @SerializedName("offer_id") val offerId: Int,
        @SerializedName("image_url") val imageUrl: String,
        @SerializedName("cash_back") val cashBack: Double
)
