package com.muneikh.c51.offer.provider

import com.muneikh.c51.offer.model.Offer
import io.reactivex.Single

interface OfferProvider {
    val offers: Single<List<Offer>>
}