package com.muneikh.c51.offer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muneikh.c51.R
import com.muneikh.c51.offer.model.Offer
import java.util.*

class OfferAdapter : RecyclerView.Adapter<OfferAdapter.BookingViewHolder>() {

    private val offers = ArrayList<Offer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferAdapter.BookingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_offer, parent, false)
        return BookingViewHolder(view)
    }

    override fun onBindViewHolder(holder: BookingViewHolder, position: Int) {
        val offer = offers[position]

        holder.offerName.text = offer.name
        holder.cashBack.text = "${offer.cashBack}"

        val context = holder.offerImage.context
        Glide.with(context).load(offer.imageUrl).into(holder.offerImage)
    }

    override fun getItemCount(): Int {
        return offers.size
    }

    fun clear() {
        val clearedCount = offers.size
        offers.clear()
        notifyItemRangeRemoved(0, clearedCount)
    }

    class BookingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val offerName: TextView = view.findViewById(R.id.offerName)
        val offerImage: ImageView = view.findViewById(R.id.image)
        val cashBack: TextView = view.findViewById(R.id.cashBack)
    }

    fun addAll(newOffers: List<Offer>) {
        val startPosition = offers.size
        offers.addAll(newOffers)
        notifyItemRangeInserted(startPosition, newOffers.size)
    }
}
