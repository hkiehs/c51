package com.muneikh.c51.offer.provider

import com.muneikh.c51.api.Api
import com.muneikh.c51.offer.model.Offer
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class OfferProviderImpl(api: Api) : OfferProvider {

    override val offers: Single<List<Offer>> = api.getOffers()
            .subscribeOn(Schedulers.io())
            .map {
                if (it.isSuccessful) {
                    it.body()!!.offers // !! because of offensive programming paradigm
                } else {
                    mutableListOf()
                }
            }
}